import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.fft as fft
from scipy.constants import c
import matplotlib.pyplot as plt
import sys
import numpy as np
import os

datapath = "C:\\Users\\adria\\Documents\\Lernmaterial\\Physik\\Solid_State_Physics\\Masterarbeit\\Data\\rawdata\\"
sys.path.append("C:\\Users\\adria\\Documents\\Lernmaterial\\Physik\\Solid_State_Physics\\Masterarbeit\\Communication\\Presentations")


def adddata(data, datafolder, filenames, channels, sweep="Freq"):
    tmpdata = pd.DataFrame()
    freqdict = {"S12": ["Unnamed: 3", "RI"], "S11": ["Hz", "S"]}
    CWdict = {"S22": ["R", "50"]}
    keydict = {"Freq": freqdict, "CW": CWdict}
    skipdict = {"Freq": 7, "CW": 8}
    xdict = {"Freq": "Frequency", "CW": "Time"}

    for file in filenames:
        rawdata = pd.read_csv(datapath+datafolder+file, sep=" ", skiprows=skipdict[sweep])
        tmpdata[xdict[sweep]] = rawdata["#"]

        for ch in channels:
                tmpkeys = keydict[sweep][ch]

                tmpdata[ch] = rawdata[tmpkeys[0]] + 1j*rawdata[tmpkeys[1]]
                tmpdata["Attenuation"+ch] = 20*np.log(np.abs(tmpdata[ch]))/np.log(10)
        tmpdata["Filename"] = np.array([file[:-4]]*len(tmpdata[xdict[sweep]]))
        data = pd.concat([data, tmpdata])
    return data


def select(data, name, fmin=0, fmax=1e12, col="Frequency"):
    plotdata = data[data["Filename"] == name]
    plotdata = plotdata[plotdata[col] > fmin]
    plotdata = plotdata[plotdata[col] < fmax]
    return plotdata

def difference(data, name1, name2, fmin=0, fmax=1e12):
    plotdata1 = select(data, name1, fmin=fmin, fmax=fmax)
    plotdata2 = select(data, name2, fmin=fmin, fmax=fmax)

    return plotdata2["AttenuationS12"]-plotdata1["AttenuationS12"]

def signalsum(data, names, fmin=0, fmax=1e12):
    sums = select(data, names[0], fmin=fmin, fmax=fmax)["AttenuationS12"]
    for name in names[1:]:
        plotdata1 = select(data, name, fmin=fmin, fmax=fmax)
        sums+=plotdata1["AttenuationS12"]

    return sums


def add_traces(traces, spectrum_idx, datafolder, startt, endt, current):
    tstamps = []
    exclude_idx = []
    labels = []
    paths = []

    tstamps.append([(startt,endt)]);
    labels.append('test');
    exclude_idx.append([]);
    paths.append(datapath+datafolder)

    # iterate over datasets

    for ii_set, tstamp in enumerate(tstamps):
        # get files
        allfiles = [f for f in os.listdir(paths[ii_set]) if (len(f) > 15 and f[8] == '_' and '.npz' in f)]
        files = []
        for tstamp_item in tstamp:
            for file in allfiles:
                currstamp = int(file[0:8])*1e6 + int(file[9:15])
                # check if the actual timestamp is part of the set
                # 1. specification as tuple: range of timestamps
                if (type(tstamp_item) == tuple):
                    strtstamp_str = tstamp_item[0].replace('_','')
                    strtstamp_len = len(strtstamp_str)
                    strtstamp = int(strtstamp_str)

                    endstamp_str = tstamp_item[1].replace('_','')
                    endstamp_len = len(endstamp_str)
                    endstamp = int(endstamp_str)

                    cmpstamp_strt = np.mod(currstamp,10**strtstamp_len)
                    cmpstamp_end = np.mod(currstamp,10**endstamp_len)

                    if ((cmpstamp_strt >= strtstamp) and ( cmpstamp_end <= endstamp)):
                        files.append(datafolder+file)

                else:
                    tgtstamp_str = tstamp_item.replace('_','')
                    tgtstamp_len = len(tgtstamp_str)
                    tgtstamp = int(tgtstamp_str)

                    cmpstamp = np.mod(currstamp,10**tgtstamp_len)

                    if (cmpstamp == tgtstamp):
                        files.append(datafolder+file)

        # sort the files according to their timestamps
        #files.sort(key = lambda x: int(x[0:8])*1e6 + int(x[9:15]))
        tmpdata = pd.DataFrame(columns=["Spectrum index", "Current", "Filename"])
        print(len(files), len(current))
        tmpdata["Filename"] = files
        tmpdata["Spectrum index"] = [spectrum_idx]*len(files)
        tmpdata["Current"] = current

        return pd.concat([traces, tmpdata], ignore_index=True)

def addnpzdata_timestamp(data, datafolder, start, endt):

    tstamps = []
    exclude_idx = []
    labels = []
    paths = []

    tstamps.append([(start,endt)]);
    labels.append('test');
    exclude_idx.append([]);
    paths.append(datapath+datafolder)

    # iterate over datasets

    for ii_set, tstamp in enumerate(tstamps):
        # get files
        allfiles = [f for f in os.listdir(paths[ii_set]) if (len(f) > 15 and f[8] == '_' and '.npz' in f)]
        files = []
        for tstamp_item in tstamp:
            for file in allfiles:
                currstamp = int(file[0:8])*1e6 + int(file[9:15])
                # check if the actual timestamp is part of the set
                # 1. specification as tuple: range of timestamps
                if (type(tstamp_item) == tuple):
                    strtstamp_str = tstamp_item[0].replace('_','')
                    strtstamp_len = len(strtstamp_str)
                    strtstamp = int(strtstamp_str)

                    endstamp_str = tstamp_item[1].replace('_','')
                    endstamp_len = len(endstamp_str)
                    endstamp = int(endstamp_str)

                    cmpstamp_strt = np.mod(currstamp,10**strtstamp_len)
                    cmpstamp_end = np.mod(currstamp,10**endstamp_len)

                    if ((cmpstamp_strt >= strtstamp) and ( cmpstamp_end <= endstamp)):
                        files.append(file)

                else:
                    tgtstamp_str = tstamp_item.replace('_','')
                    tgtstamp_len = len(tgtstamp_str)
                    tgtstamp = int(tgtstamp_str)

                    cmpstamp = np.mod(currstamp,10**tgtstamp_len)

                    if (cmpstamp == tgtstamp):
                        files.append(file)

        # sort the files according to their timestamps
        files.sort(key = lambda x: int(x[0:8])*1e6 + int(x[9:15]))

        # get all data
        s_x = []
        s_y = []
        temp = []
        field = []
        valid = []
        ignored = []
        for ii,file in enumerate(files):

            fullpath = os.path.join(paths[ii_set],file)

            loadz = np.load(fullpath)


            tmpdata = pd.DataFrame()
            tmpdata["Frequency"] = loadz['s_x']
            tmpdata["S22"] = loadz['s_y']
            tmpdata["Filename"] = np.array([file[:-4]]*len(tmpdata["Time"]))
            #tmpdata["Current"] = loadz["field"]*np.ones(len(tmpdata["Time"]))
            #temp.append(loadz['temp'])
            #field.append(loadz['field'])
            data = pd.concat([data, tmpdata])


            valid.append(ii)
            if (ii in exclude_idx[ii_set]):
                s_x.pop()
                s_y.pop()
                valid.pop()
                ignored.append(ii)
    return data

def addnpzdata(data, datafolder, name):
    allfiles = [f for f in os.listdir(datafolder) if (name in f and '.npz' in f)]

    for ii, file in enumerate(allfiles):

        fullpath = os.path.join(datafolder,file)

        loadz = np.load(fullpath)


        tmpdata = pd.DataFrame()
        tmpdata["Time"] = loadz['s_x']
        tmpdata["S22"] = loadz['s_y']
        tmpdata["Filename"] = np.array([file[:-4]]*len(tmpdata["Time"]))
        #tmpdata["Current"] = loadz["field"]*np.ones(len(tmpdata["Time"]))
        #temp.append(loadz['temp'])
        #field.append(loadz['field'])
        data = pd.concat([data, tmpdata])

    return data
