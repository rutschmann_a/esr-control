# -*- coding: utf-8 -*-

# debugging the field ctrl

import numpy as np
import matplotlib.pyplot as plt



# set field versus field readback
vals = []
vals.append([1,1799])
vals.append([2,3557])
vals.append([3,5337])
vals.append([4,7160])
vals.append([5,8788])
vals.append([6,10370])
vals.append([7,11726])
vals.append([8,12863])
vals.append([9,13833])
vals.append([10,14655])
vals.append([11,14655])

v = np.array(vals)

d_x = v[:,0]
d_y = v[:,1]

ids = d_x<6

linpol = np.polyfit(d_x[ids],d_y[ids], 1)

plt.figure(1); plt.clf()

plt.plot(d_x,np.polyval(linpol,d_x),':')
plt.plot(d_x,d_y,marker='o')


plt.xlabel('SET field [G]')
plt.ylabel('Field readback [G]')