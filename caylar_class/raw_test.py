import socket
from time import sleep


HOST = '129.129.98.110' # Enter IP or Hostname of your server
PORT = 1234 # Pick an open Port (1000+ recommended), must match the server port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

# Lets loop awaiting for your input
# while True:
#     command = input('Enter your command: ')
#     s.send(command.encode())
#     reply = s.recv(1024).decode()
#     if reply == 'Terminate':
#         break
#     print(reply)
    
    

command = "*IDN?\r\n"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

1/0

command = u"This is important"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)


command = u":Phase1 0"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

command = u":Phase2 90"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

command = u":Phase1?"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

command = u":Phase2?"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

# and a wrong command, which will here give a long response (later that will be short or long, controlled by the verbose mode)
command = u":Phase1 0, 90"
s.send(command.encode())
reply = s.recv(1024).decode()
print(reply)
sleep(0.1)

s.close()
