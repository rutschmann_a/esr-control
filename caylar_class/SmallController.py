from socket import timeout
from time import sleep
import PowerSupplyCaylarLib as Magnet


class Controller():
    magnet = Magnet.CaylarPowerSupply
    USER_PSUPPLY_IP = '129.129.98.110'
    USER_PSUPPLY_PORT = 1234 

    timeout = 5

    def __init__(self,ip=USER_PSUPPLY_IP,port=USER_PSUPPLY_PORT):
        super().__init__()
        self.magnet = Magnet.CaylarPowerSupply(self.timeout)
        self.USER_PSUPPLY_IP =  ip
        self.USER_PSUPPLY_PORT = port


    def connect(self,):
        print("Trying to connect to " + self.USER_PSUPPLY_IP + " (port " + self.USER_PSUPPLY_PORT+") ...")

        try:
            self.magnet.connect(self.USER_PSUPPLY_IP,self.USER_PSUPPLY_PORT)

        except Exception as e:
            print("\nCould not connect. Error:\n")
            print(e)
        else:
            print("Successfully connected to the power supply!\n")
            print("Power supply info:")
            print("---------------------------")
            print(" - IDN: " + self.magnet.getIDN())


    def getTemp(self):
        print("\n - Températures infos:")
        print("    - Rack temp = " + self.magnet.getRackTemp().split(" ")[1])
        print("    - Box temp = " + self.magnet.getBoxTemp().split(" ")[1])
        print("    - ADC/DAC temp = " + self.magnet.getAdcDacTemp().split(" ")[1])

    def getField(self):
        print("\n - Power infos:")
        power_state = int(self.magnet.getPowerState().split(" ")[1])
        selector_pos = int(self.magnet.getSelectorPos().split(" ")[1])
        print("    - Current remote from " + ("front potentiometer", "internal DAC", "external voltage source")[selector_pos])
        print("    - Power is " + ("off", "on")[power_state])
        print("    - Output tension = " + self.magnet.getVoltage().split(" ")[1])
        print("    - Output current = " + self.magnet.getCurrent().split(" ")[1])
        print("    - Field = " + self.magnet.getField())



    def SetField(self,field):
        print("\n - Setting field to: " +  field + " G...")
        response = self.magnet.setField(field)
        print("Magnet responded: " + response)

        try:
            while True:
                print("\n - Power infos:")
                print("    - Output current = " + self.magnet.getCurrent().split(" ")[1])
                print("    - Field = " + self.magnet.getField())
                sleep(0.5)

        except KeyboardInterrupt:
            print("Reading stopped.")


    def disconnect(self):
        self.disconnect()

