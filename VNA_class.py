# -*- coding: utf-8 -*-

import vxi11

import sys

import time
import struct
import numpy as np
import matplotlib.pyplot as plt

import os
import os.path




class VNA:
    def __init__(self, start=2.0, stop=6.0, points=1601):
        # E5810a address/name
        adapter="129.129.128.113"
        adapter="129.129.140.76"
        adapter="129.129.121.120"

        # GPIB number of VNA
        n_gpib = 16


        self.inst=vxi11.Instrument(adapter,'hpib,{0}'.format(n_gpib))
        self.inst.open()

        print(self.inst.ask("*IDN?\r\n"))

        # set the span
        self.set_xaxis(start, stop, points)

        time.sleep(0.25)

        # read back the span
        self.get_xaxis()

    def __del__(self):
        self.inst.close()


    def set_xaxis(self, start, stop, points):
        self.inst.write('star {:e};\r\n'.format(start*1e9))
        self.inst.write('stop {:e};\r\n'.format(stop*1e9))
        self.inst.write('poin {:e};\r\n'.format(points))

    def get_xaxis(self):

        self.start = float(self.inst.ask('star?;\r\n'))
        self.stop = float(self.inst.ask('stop?;\r\n'))
        self.points = int(float(self.inst.ask('poin?;\r\n')))

        return np.linspace(self.start, self.stop, self.points)

    # method to retrieve the data
    def get_trace(self):
        cmd = 'outpdata;\r\n'
        self.inst.write('form2;\r\n')

        # perform the measurement
        self.inst.ask('OPC?; SING;\r\n')

        data = self.inst.ask_raw(cmd.encode())


        count = struct.unpack_from('>h',data, 2)[0]
        fmt = '>{:d}f'.format(count//4)
        s_pairs = struct.unpack_from(fmt, data, 4)
        s = np.empty(len(s_pairs)//2, 'complex')
        s.real, s.imag = s_pairs[::2], s_pairs[1::2]

        self.inst.write('CONT;\r\n')

        return s

    def save_trace(self, folder, field=0, temp=300):
        tstamp = time.strftime("%Y%m%d_%H%M%S")
        fname = '{}_{:.1f}K_{:.1f}G'.format(tstamp,temp,field)
        fname = fname.replace('.','p')

        if os.path.isdir(folder) == False:
            os.mkdir(folder)

        self.s_x = self.get_xaxis()
        self.s_y = self.get_trace()
        np.savez(folder+fname, s_x = self.s_x, s_y = self.s_y)

        print('Stored under: '+fname)

    def plot_last(self, fignum = 1):

        if hasattr(self,'s_x'):
            plt.figure(fignum)
            plt.clf()
            plt.plot(self.s_x, abs(self.s_y))


if __name__ == '__main__':
    vna = VNA()
